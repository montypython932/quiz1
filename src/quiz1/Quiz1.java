package quiz1;

import java.util.Scanner;

public class Quiz1 {

    public static void main(String[] args) {
        String username, password;
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter your username: ");
        username = input.nextLine();
        System.out.println("Please enter your password: ");
        password = input.nextLine();
        System.out.println("Hello " + username + ", Welcome to CSC 200! Your password is: " + password);
    }
    
}
